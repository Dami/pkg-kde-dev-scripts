Howto use the tools in this directory
=====================================

git config
----------
some scripts depend on the following configuration being in your `.gitconfig`:
```
[url "https://salsa.debian.org/qt-kde-team/"]
        insteadOf = qt-kde-team:
[url "git@salsa.debian.org:qt-kde-team/"]
        pushInsteadOf = qt-kde-team:
[url "https://invent.kde.org/"]
        insteadOf = kde:
[url "git@invent.kde.org/"]
        pushInsteadOf = kde:
```

git repo setup
--------------
* make sure you have all git checkouts in a directory BASE/kde
* alternatively the `frameworks/plasma/pim_tier_graph.py` scripts will clone the
  git repos from salsa into `BASE/`.
* directory names of the checkouts default to be *without* .git extension, but
  if they are already there *with* .git they will be used. If both are present,
  an exception will be raised.
* directory names must match the git repo remote
* `pkg-kde-dev-tools` and [`pkg-kde-jenkins`](https://salsa.debian.org/qt-kde-team/pkg-kde-jenkins) can be anywhere, independent of name

config.yml
----------
copy `config-SAMPLE.yml` to `pkg-kde-dev-scripts/function_collection/config.yml` and edit:
* `name`, `email` obvious
* `basedir` is the above `BASE`, which contains a directory "kde" which contains the git repos
* `pkg-kde-jenkins`: path to the checkout, can be anywhere

only nessary for some operations / can be skipped for most users:

* `repopath`: path to a reprepro directory (Can be used to have additional local packages for sbuild, helps to build all tiers on your PC)
* `decopy`: Checkout of [`pkg-kde-jenkins`](https://salsa.debian.org/debian/decopy) to use unreleased version.
* `salsa_token`: [Salsa private access token](https://salsa.debian.org/-/profile/personal_access_tokens) with api scope.
* `repomentadata`: checkout to KDE upstream [repo-metadata repository](https://invent.kde.org/sysadmin/repo-metadata)


General usage
-------------

The idea is always to use the scripts in an interactive python shell (Recommandation is ipython3).
In any case it should start without any warning or error.

If you want just to work with one package:
```
cd PATHTO_PACKAGE
ipython3 -i <PATHTO>/function_collection/functions.py
```
* `pkg` an instance of Package representing the current package.


to work with KDE Frameworks:
```
ipython3 -i <PATHTO>/function_collection/framework_management.py
```
* `tiers` a list with all the source packages in their corresponding tier
* `binaryPackages` a set of all created binary packages
* `packages` a dictornary of the form `source name`: `Package`

and to woh with KDE PIM:
```
ipython3 -i <PATHTO>/function_collection/pim_management.py
```
* `tiers` a list with all the source packages in their corresponding tier
* `binaryPackages` a set of all created binary packages
* `packages` a dictornary of the form `source name`: `Package`

if you work with a set of packages you can use several patterns:

* either get one package via:
```
pkg = packages['NAME']
<some_command>(pkg, ...)
```

* or iterate over all in one tier e.g. first tier:
```
for pkg in tiers[0]:
    <some_command>(pkg, ...)
```

* or iterate over all packages in all tiers:
```
for pkg in itertools.chain(*tiers):
    <some_command>(pkg, ...)
```

* or iterate over every loaded packages:
```
for pkg in packages.values():
    <some_command>(pkg, ...)
```

We can use `map` if we want to run only one command for a set of packages. E.g. check git status run git push if needed for all Framework/Pim packages:
```
list(map(checkGitStatus, itertools.chain(*tiers)))
```



update to new version
----------------------
from the python prompt:
```
simple_package(pkg, Version('5.77.0-1'))
```

or run:
```
cd PATHTO_PACKAGE
python3  <PATHTO>/function_collection/simple_pkg.py 5.77.0-1
```


check git status
----------------
```
checkGitStatus(pkg, False)
```
Second parameter enabled/disables git pushes.

Build source package
--------------------
```
pkg.dpkgBuildpackage()
```

Release
-------
```
release(pkg, 'experimental')
```
opens the changelog to be able to review chalngelog before releasing. And also tag the commit.


Upload
------
```
dput(pkg)
```
Normally you will upload to the offical Debian queue.
If you have a local builder and want to upload to another place you can set `options.dput_opt`, before you run `dput`.
See the manpage of dput for all options to set.
E.g I have a local builder called BUILDER and want to force push:
```
options.dput_opt = ["-f", "BUILDER"]
```

Symbol updates
--------------
```
#download all build logs
getBuildlogs(pkg)
#update symbols
updateSymbols(pkg,"5.77.0")
```

Various comments
----------------
* if you want to update experimental, first checkout the "debian/experimental" branch
  for all packages. Also, don't forget to merge master before starting an update
  session, to keep merging back easy.

