import pydot
import pathlib

from functions import *
from functions_plasma import *

import config
import salsa
from simple_pkg import simple_package
import check_old_depdendency

KDEDIR = BASEDIR/"pim"

#Read tier data
tiers=[]
dotpath = sorted(config.tierdata.glob('pim.*.tier.dot'),reverse=True)[0]
version = ".".join(dotpath.name.split(".")[1:4])
print(f"INFO: using kdepim {version} version data")
for subgraph in pydot.graph_from_dot_file(dotpath)[0].get_subgraph_list():
    tier=set()
    for node in subgraph.get_nodes():
        pkg_name = node.get_name()[1:-1]
        pkg_path = KDEDIR/pkg_name
        control = pkg_path/"debian/control"
        pkg = getPackage(control)
        if not pkg:
            print(f"Failed to find {pkg_name}")
            continue
        tier.add(pkg)
    tiers.append(tier)

binaryPackages=set()
for p in itertools.chain(*tiers):
    binaryPackages |= set(i.get("Package") for i in p.controlParagraphs() if i.get("Package"))
