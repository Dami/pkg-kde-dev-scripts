# SPDX-FileCopyrightText: 2022 Sandro Knauß <hefee@debian.org>
# SPDX-License-Identifier: LGPL-2.0-or-later

import argparse
import logging
import pathlib
import subprocess
import sys
import typing

import yaml

from bs4 import BeautifulSoup
import git
import requests

from . import config, dot, functions

logger = logging.getLogger(__name__)


class PackageException(Exception):
    pass

class EmptyUpstreamPackages(Exception):
    pass

class BuildGraph:
    """class to represent a build graph of debian packages."""

    def __init__(self):
       self.packages = {}
       self.binary_packages = {}    # binary package -> source package

    def add_package(self, package):
        """add a package to the build graph and updates
        the binary_packages dictionary.
        """
        self.packages[package.name] = package
        for pkg in package.buildPackages:
            self.binary_packages[pkg] = package

    def simplyfied_graph(self):
        """creates a simplyfied graph.
        * removes all external depdencies (not inside binary_packages)
        * replaces binary package name with source name
        """
        graph = {}
        logger.info("Simplify dependency graph ...")
        for name, pkg in self.packages.items():
            sDepends = set()
            for rel in pkg.buildDepends:
                rel_name = functions.relName(rel)
                if rel_name not in self.binary_packages:
                    continue
                sDepends.add(self.binary_packages[rel_name])
            graph[name] = sDepends
        return graph


class BundleData:
    def __init__(self, bundle_name, version, tier_name, upstream_packages):
        self.name = bundle_name
        self.version = version
        self.tier_name = tier_name

        self.upstream_packages = upstream_packages
        self.upstream_packages.discard("")
        self.fail_empty_upstream()

    def fail_empty_upstream(self):
        if not self.upstream_packages:
            raise EmptyUpstreamPackages(f"No {self.name} found for version {self.version}")

    @property
    def basedir(self):
        return config.BASEDIR/self.name

    def config(self):
        """return dict of bundle config"""
        with (config.tierdata/f"{self.name}_upstream.yml").open() as f:
            return yaml.safe_load(f)

    def debian_packages(self):
        """renames upstream to debian package names."""
        cfg = self.config()
        for p in self.upstream_packages:
            if p in cfg.get("ignore"):
                continue
            yield cfg.get("replace").get(p, p) if cfg.get("replace") else p

    def iter_packages(self, clone:bool=False):
        """returns a Generator of all debian packages"""
        for p in self.debian_packages():
            yield get_package_by_path(self.basedir/p, clone)

    def clone_missing_repos(self):
        """clone packages when the package cannot be found."""
        failed = False
        cfg = self.config()
        replace_url = cfg.get('replace_url', dict())
        logger.info("Check and clone missing repositories ...")
        for i in self.debian_packages():
            try:
                repo_url = replace_url.get(i, f'kde/{i}.git')
                clone_package(self.basedir/i, repo_url)
            except PackageException as e:
                logger.error(str(e))
                failed = True
        if failed:
            raise PackageException("Cloning failed for at least one repository.")

    def build_graph(self) -> BuildGraph:
        """generates a BuildGraph"""
        build_graph = BuildGraph()
        logger.info("Generate dependency graph ...")
        for package in self.iter_packages():
            build_graph.add_package(package)
        return build_graph

    def generate_tier_graphs(self):
        """generate all tier graph files."""
        build_graph = self.build_graph()
        t = dot.TierGraph(build_graph.simplyfied_graph())
        print(t)
        dotfile = config.tierdata/f"{self.name}.{self.version}.tier.dot"
        logger.info(f"generating tier files for {self.version}...")
        generate_files(build_graph.packages, t, self.tier_name, dotfile)

    def __repr__(self):
        return f"<BundleData({self.name}, {self.version})>"


def package_list_from_index(url: str):
    """returns a generator of all tar.xz links.
    @url: an url
    """
    logger.info(f"Getting package list from {url}")
    r = requests.get(url)
    soup = BeautifulSoup(r.text, features="lxml")
    for a in soup.find_all('a'):
        if a['href'].endswith('.tar.xz'):
            yield a


def generate_files(build_graph:BuildGraph, tier_graph:dot.TierGraph, tier_name:str, stem:pathlib.Path) -> None:
    """ Generates tier files and an image.
    uses pathlib.with_suffix to generate the different files
    * bashfile (suffix .sh)
    * dotfile (suffix .dot)
    * visual image of dot file (suffix .png)
    @stem: stem path to generate files"""

    dotfile = stem.with_suffix(".dot")
    dotfile.write_text(tier_graph.createGraph(tier_name))

    bashfile = stem.with_suffix(".sh")
    bashfile.write_text(tier_graph.createBashArray(tier_name.upper(), build_graph))

    pngname = stem.with_suffix(".png")
    subprocess.check_call(["dot", "-T","png", "-o", pngname, dotfile])


def get_package_path(path: pathlib.Path) -> typing.Optional[pathlib.Path]:
    """reutrn a valid path to package.
    checks if path or path.git exists.
    """
    path_git = path.with_suffix(".git")
    if path.exists() and path_git.exists():
        raise PackageException(f"Both {path} and {path_git} exist, don't know which to use.")
    if path.exists():
        return path
    elif path_git.exists():
        return path_git


def get_package_by_path(path: pathlib.Path, clone : bool = False):
    """returns a package from a path.

    * will clone the repo if the path does not exist.
    * will raise PackageException if it does not work like expected.
    """
    name = path.name
    pp = get_package_path(path)
    if pp is None and clone:
        clone_package(path)
    pp = get_package_path(path)
    if not pp or not pp.exists():
        raise PackageException(f"This should not happen unless the git clone of {name} failed!")
    control = pp/'debian/control'
    pkg = functions.getPackage(control)
    if pkg is None:
        raise PackageException(f"Cannot parse/find package at {control}")
    return pkg


def clone_package(package_path: pathlib.Path, repo_url: typing.Optional[str] = None) -> None:
    """clone the package if it does not exist.
    @package_path: a path with name of the package.
    @repo_url: the url to clone from (default kde/name.git)
    """
    if get_package_path(package_path):
        return
    name = package_path.name
    if not repo_url:
        repo_url = f"kde/{name}.git"
    logger.info(f"Cloning {name} ...")
    git.Repo.clone_from(f"qt-kde-team:{repo_url}", package_path)

def main(name:str, tier_name:str, func) -> None:
    """main logic of generate tier graphs and data.
    @name: name of the bundle
    @tier_name: name for the tiers
    @func: function that returns a list of upstream packages, will get the requested version as parameter
    """

    parser = argparse.ArgumentParser()
    parser.add_argument('version', help = 'version to create tier graph')
    args = parser.parse_args()

    version = args.version
    upstream_packages = func(version)

    try:
        data = BundleData(name, version, tier_name, upstream_packages)
    except EmptyUpstreamPackages:
        logger.error(f"No {name} found for version {version}")
        sys.exit(1)

    try:
        data.clone_missing_repos()
    except PackageException:
        sys.exit(1)

    data.generate_tier_graphs()
    logger.info("done")
