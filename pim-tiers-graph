#! /usr/bin/env python3
# SPDX-FileCopyrightText: 2022 Sandro Knauß <hefee@debian.org>
# SPDX-License-Identifier: LGPL-2.0-or-later

import argparse
import logging
import re
import subprocess
import yaml

from function_collection import config, tier

logging.basicConfig(level=logging.INFO,
                    format='%(levelname)s: %(message)s')

logger = logging.getLogger()


def fix_pkg_names():
    pkg_map = {"libkf5grantleetheme": "grantleetheme", "libkf5libkdepim": "libkdepim", "libkf5libkleo": "libkleo",
               "libkf5pimcommon": "pimcommon", "libkf5calendarsupport": "calendarsupport",
               "libkf5gravatar": "libgravatar", "libkf5ksieve": "libksieve", "libkf5mailimporter": "mailimporter",
               "kf5-messagelib": "messagelib", "libkf5eventviews": "eventviews",
               "libkf5incidenceeditor": "incidenceeditor", "libkf5mailcommon": "mailcommon"}
    parser = argparse.ArgumentParser()
    parser.add_argument('version', help = 'version to create tier graph')
    args = parser.parse_args()
    version = args.version

    filename = "tierdata/pim." + version + ".tier.dot"
    with open(filename, 'r+') as file:
        lines = file.readlines()
        file.seek(0)
        file.truncate(0)
        for line in lines:
            l = line
            for name, salsa_name in pkg_map.items():
                if name in l:
                    l = re.sub(name, salsa_name, line)
                    break
            file.write(l)

        file.close()

    filename = "tierdata/pim." + version + ".tier.sh"
    with open(filename, 'r+') as file:
        lines = file.readlines()
        file.seek(0)
        file.truncate(0)
        for line in lines:
            l = line
            for name, salsa_name in pkg_map.items():
                if salsa_name in l:
                    l = re.sub(salsa_name, name, line)
                    break
            file.write(l)

        file.close()


def upstream_packages(version:str) -> set[str]:
    repo_metadata=config.BASEDIR/'repo-metadata'

    if repo_metadata.exists():
        subprocess.check_call(['git', 'pull'], cwd=repo_metadata)
    else:
        subprocess.check_call(['git', 'clone', 'kde:sysadmin/repo-metadata'], cwd=config.BASEDIR)

    pp = set()
    for pkg in (repo_metadata/"projects/pim").iterdir():
        if pkg.name == "metadata.yaml":
            continue
        with (pkg/'metadata.yaml').open() as f:
            metadata = yaml.safe_load(f)
        if metadata.get('repoactive', False):
            pp.add(pkg.name)
    return pp

tier.main('pim', "pimTiers", upstream_packages)
# Dirty hack for now to fix the wrong names in the dot and sh files
fix_pkg_names()
