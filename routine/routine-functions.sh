#!/bin/bash

is_dep5_copyright () {
  local is_dep5
  grep -e "^Format:.*https\?://www.debian.org/doc/packaging-manuals/copyright-format/1.0/" debian/copyright > /dev/null 2>&1
  is_dep5=$(( $? ? 0 : 1 ))
  echo ${is_dep5}
}

# Try getting the project description from the KDE project API using in this order:
# - Upstream-Name field from d/copyright
# - Source package name from d/changelog
# - Last part of the Salsa repo origin URL
kde_project_api_get_json () {
  local deb_source_path=$1
  if [ -z "${deb_source_path}" ] ; then
    deb_source_path=${PWD}
  fi
  local upstream_name
  if upstream_name=$(\grep -e "^Upstream-Name:" ${deb_source_path}/debian/copyright) ; then
    upstream_name="${upstream_name##Upstream-Name: }"
    upstream_name="${upstream_name,,}"
    project_json=$(curl -s https://projects.kde.org/api/v1/identifier/${upstream_name})
  fi
  if [ -z "${project_json}" ] ; then
    pkg=$(dpkg-parsechangelog -S Source)
    project_json=$(curl -s https://projects.kde.org/api/v1/identifier/${pkg})
  fi
  if [ -z "${project_json}" ] ; then
    salsa_remote_basename=$(git -C "${deb_source_path}" remote get-url origin | sed -e "s=.*/\([^/]\+\).git=\1=")
    project_json=$(curl -s https://projects.kde.org/api/v1/identifier/${salsa_remote_basename})
  fi
  if [ -n "${project_json}" ] ; then
    echo "${project_json}"
  else
    echo "could not find project in KDE API, neither as '${upstream_name}' from d/copyright, as '${pkg}' from d/changelog or as '${salsa_remote_basename}' from the Salsa repo name"
    exit 1
  fi
}
