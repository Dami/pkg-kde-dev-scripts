#!/bin/bash
set -e
set -o pipefail
pkg=$(head -1 debian/changelog | cut -d" " -f1)
declare -a uscan_res
if ! uscan_output=$(LANG=C uscan | \grep "uscan: Newest version of" | sed -e "s/uscan: Newest version of \([^[:space:]]\+\) on remote site is \([^,]\+\), local version is \([^[:space:]]\+\)[[:space:]]*/\1 \2 \3/") ; then
  echo "INFO: [debian/changelog] could not find new upstream version with uscan, skipping upstream version update"
  exit 0
fi
uscan_res=(${uscan_output})

echo "INFO: uscan found new version ${uscan_res[1]} for ${uscan_res[0]}"
pushd .
cd ..
orig_pkg_symlink=$(find . -mindepth 1 -maxdepth 1 -name "${pkg}_${uscan_res[1]}.orig.tar*" -not -name "*.asc")
orig_pkg_filename=$(readlink ${orig_pkg_symlink})
escaped_version=$(echo "${uscan_res[1]}" | sed -e "s/\./\\\./g")
orig_version_suffix=$(expr match "$orig_pkg_filename" ".*\(-${escaped_version}.*\)")
orig_pkg_basename="${orig_pkg_filename%%${orig_version_suffix}}"

if [ ! -d "${orig_pkg_basename}-${uscan_res[1]}" ] ; then
  echo "INFO: extracting ${orig_pkg_filename} to ${PWD}"
  tar xfa "${orig_pkg_filename}"
fi
popd
# Needed twice for complete cleanup
git clean -d -f
git clean -d -f
echo "INFO: extracting ${orig_pkg} to ${PWD}"
tar xva --strip-components=1 -f ../${orig_pkg_filename}
epoch=$(head -1 debian/changelog | sed "s/${uscan_res[0]}[[:space:]]\+(\([[:digit:]]\+:\)*[^)]\+).*/\1/")
msg="New upstream release (${uscan_res[1]})."
dch -t --no-auto-nmu -v "${epoch}${uscan_res[1]}-1" "${msg}"
git add debian/changelog
git commit -m "${msg}"
git diff HEAD~..HEAD

echo "INFO: installing build dependencies"
if ! LANG=C apt-get -s build-dep . | grep -q "0 newly installed" ; then
  sudo apt build-dep .
fi

if ! debuild -us -uc ; then
  build_failed=1
fi

if [ -z "${build_failed}" ] ; then
  lintian -I --pedantic
#  lintian -I --pedantic --color=always | less -R
fi

printf "\nPlease review upstream diff for example with:\n"
echo "  diff --color=always -u -r -N ../${orig_pkg_basename}-${uscan_res[2]} ../${orig_pkg_basename}-${uscan_res[1]} | less -R"
echo "Looking for CMakeLists changes and Copyright changes is a must !"
#set +o pipefail
#diff --color=always -u -r -N ../${orig_pkg_basename}-${uscan_res[2]} ../${orig_pkg_basename}-${uscan_res[1]} | less -R
#set -o pipefail

# Look for QML imports
printf "\nLooking for QML imports, corresponding runtime deps need to be added to d/control\n"
if ! find . -name "*.qml" -exec grep --color=always -e "^[[:space:]]*import" {} \; | sed -e "s/\.[[:digit:]]\+$/.x/" | sed -e "s/\.[[:digit:]]\+\([^\.]\)/.x\1/" | sort | uniq | less -R ; then
  echo "Found: NONE"
fi

if [ "${build_failed}" ] ; then
  >&2 echo "ERROR: build failed"
  exit 1
fi
