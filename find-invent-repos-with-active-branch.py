#!/usr/bin/python3
import getopt, sys
import gitlab
import dateutil.parser
import datetime

def usage():
    print("Usage:")
    print("    {} [OPTION...] -t PRIVATE_TOKEN -g INVENT_BASE_GROUP INVENT_GIT_BRANCH".format(sys.argv[0]))
    print()
    print("Argument:")
    print("    INVENT_GIT_BRANCH  the name of the invent branch to look for")
    print()
    print("Options:")
    print("    -g INVENT_BASE_GROUP, --limit-to-group=INVENT_BASE_GROUP")
    print("        Group path or ID where project will be searched for the given active branch.")
    print("    -t PRIVATE_TOKEN, --private-token=PRIVATE_TOKEN")
    print("        API token to be used to connect to invent.kde.org.")
    print()
    print("Example:")
    print("    {} -t my_invent_gitlab_token -g Plasma Plasma/5.18".format(sys.argv[0]))

# Branch is considered inactive if it has no commit in the last 3 months
# (4 months = KDE release cycle)
DAYS_BEFORE_BRANCH_INACTIVE=4*30

def main():
    try:
        opts, args = getopt.getopt(sys.argv[1:], "ht:g:", ["help", "private-token", "limit-to-group="])
    except getopt.GetoptError as err:
        # print help information and exit:
        print(err)  # will print something like "option -a not recognized"
        usage()
        sys.exit(2)
    private_token = None
    limit_to_group = None
    limit_to_group_id = None
    invent_branch = None
    for o, a in opts:
        if o in ("-h", "--help"):
            usage()
            sys.exit()
        elif o in ("-t", "--private-token"):
            private_token = a
        elif o in ("-g", "--limit-to-group"):
            limit_to_group = a
        else:
            assert False, "unhandled option"

    if len(args) < 1 or not private_token or not limit_to_group:
        usage()
        sys.exit(2)
    else:
        invent_branch = args[0]

    invent = gitlab.Gitlab('https://invent.kde.org', private_token=private_token)
    invent.auth()

    today = datetime.date.today()


    projects = invent.groups.get(limit_to_group).projects.list(all=True, include_subgroups=True)

    for group_project in projects:
        project = invent.projects.get(group_project.id)
#        print('Processing project {} (ID: {})'.format(project.name, project.id))
        for branch_to_find in project.branches.list():
            if branch_to_find.name == invent_branch:
                last_commit_date = dateutil.parser.parse(branch_to_find.commit['authored_date']).date()
                if today - last_commit_date < datetime.timedelta(days=DAYS_BEFORE_BRANCH_INACTIVE):
                    print(project.http_url_to_repo)
    
if __name__ == "__main__":
    main()
